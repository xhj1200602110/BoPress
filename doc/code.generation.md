# 代码生成 #
系统自带一个从数据表生成代码的插件，提供生成WordPress DataTable类、JQuery DataTables、Bootstrap Forms。
此插件也提供了扩展点，方便编写自己的代码片段生成功能。

    from bopress.hook import  add_code_generation_menu_page, add_filter, add_code_generation_submenu_page

	add_code_generation_menu_page("代码", "代码", "code-generic")
	add_code_generation_submenu_page("code-generic", "WordPress DataTable", "WP DataTable", "code-wordpress")
	add_code_generation_submenu_page("code-generic", "JQuery DataTables", "JQuery DataTables", "code-jquery-datatables")
	add_code_generation_submenu_page("code-generic", "Html Forms", "Html Forms", "code-html-forms")

`add_code_generation_menu_page`、`add_code_generation_submenu_page` 函数可增加一个带有数据库连接管理，数据库下的数据表选择及表字段过滤、参数的菜单页面。

在页面添加一个Button来触发渲染一个代码生成模板。Button名称`AdminLTE`，点击Button，页面将把所有参数传递到后台渲染模板`code/forms-adminlte.html`，模板文件位于templates文件夹下。

    def htmlforms_code_generation_properties_buttons(btns, screen_id, hanlder):
	    if screen_id == "code-html-forms":
	        btns.append(("AdminLTE", "code/forms-adminlte.html"))
	    return btns

	add_filter("bo_code_generation_buttons", htmlforms_code_generation_properties_buttons)

为模板添加模板生成参数，注意表单控件名称需以`opt_`开头。

    def htmlforms_datatables_code_generation_properties_form(html_form_str, current_screen):
	    if current_screen.id == "code-html-forms":
	        html_form_str = """
	                <div class="form-group">
	                  <label>列数</label>
	                  <select class="form-control" name="opt_column_nums">
	                    <option value="1">1列</option>
	                    <option value="2">2列</option>
	                    <option value="3">3列</option>
	                    <option value="4">4列</option>
	                  </select>
	                </div>
	        """
	    return html_form_str

	add_filter("bo_code_generation_properties_form", htmlforms_datatables_code_generation_properties_form)

至此，传递到模板将包括以下参数

1. TableName
2. table_name
3. exclude_columns
4. search_columns
5. order_columns
6. freeze_columns
7. columns
8. primary_key
9. option

参数具体使用请参考`code/forms-adminlte.html`生成逻辑。

`columns`列信息集合

`option`即属性表单值集合

例如访问上面属性表单值 `int(option['column_nums'])`。