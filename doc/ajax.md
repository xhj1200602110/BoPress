# AJAX #
在插件中定义

    def func(handler):
		a = handler.get_argument('some_post_v')
		handler.render_json(a)
	add_action("bo_api_post_some_api_name", func)

> handler: `bopress.handler.ApiHandler`

Html页面请求

    $.post('{{api("some_api_name")}}', params, function(rsp){},"json");

或者

    $.post('/bopress/api/some_api_name', params, function(rsp){},"json");

实现AJAX的扩展点规则`bo_api_(httpmethod)_(apiname)`，`apiname`正则适配`\w+`，所以只能是字母、数字、下划线组合。

有效的`httpmethod`:

1. `bo_api_get`
2. `bo_api_post`
3. `bo_api_put`
4. `bo_api_delete`
5. `bo_api_head`
6. `bo_api_options`
7. `bo_api_patch`